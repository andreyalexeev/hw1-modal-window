import React, { useState } from 'react';
import Button from './components/Button';
import { Modal, ModalHeader, ModalFooter, ModalClose, ModalBody } from './components/Modal';
import './components/styles/index.scss';

const App = () => {
  const [isFirstModalOpen, setIsFirstModalOpen] = useState(false);
  const [isSecondModalOpen, setIsSecondModalOpen] = useState(false);

  const openFirstModal = () => setIsFirstModalOpen(true);
  const closeFirstModal = () => setIsFirstModalOpen(false);

  const openSecondModal = () => setIsSecondModalOpen(true);
  const closeSecondModal = () => setIsSecondModalOpen(false);

  return (
    <div className="App">
      <Button classNames="primary" onClick={openFirstModal}>
        Open first modal
      </Button>
      <Button classNames="primary" onClick={openSecondModal}>
        Open second modal
      </Button>

      {isFirstModalOpen && (
        <Modal onClose={closeFirstModal}>
          <ModalHeader>
            <h2>First Modal</h2>
            <ModalClose onClick={closeFirstModal} />
          </ModalHeader>
          <ModalBody>
            <p>This is the first modal content.</p>
          </ModalBody>
          <ModalFooter firstText="Close" firstClick={closeFirstModal} />
        </Modal>
      )}

      {isSecondModalOpen && (
        <Modal onClose={closeSecondModal}>
          <ModalHeader>
            <h2>Second Modal</h2>
            <ModalClose onClick={closeSecondModal} />
          </ModalHeader>
          <ModalBody>
            <p>This is the second modal content.</p>
          </ModalBody>
          <ModalFooter firstText="Close" firstClick={closeSecondModal} />
        </Modal>
      )}
    </div>
  );
};

export default App;
