import './styles/Modal.scss';

const ModalWrapper = ({ children, onClose }) => (
    <div className="modal-wrapper" onClick={onClose}>
        <div className="modal-content" onClick={(e) => e.stopPropagation()}>
            {children}
        </div>
    </div>
);

const ModalHeader = ({ children }) => <div className="modal-header">{children}</div>;

const ModalFooter = ({ firstText, secondaryText, firstClick, secondaryClick }) => (
    <div className="modal-footer">
        {firstText && <button onClick={firstClick}>{firstText}</button>}
        {secondaryText && <button onClick={secondaryClick}>{secondaryText}</button>}
    </div>
);

const ModalClose = ({ onClick }) => (
    <button className="modal-close" onClick={onClick}>
        &times;
    </button>
);

const ModalBody = ({ children }) => <div className="modal-body">{children}</div>;

const Modal = ({ children, onClose }) => (
    <ModalWrapper onClose={onClose}>
        {children}
    </ModalWrapper>
);

export { Modal, ModalHeader, ModalFooter, ModalClose, ModalBody };
