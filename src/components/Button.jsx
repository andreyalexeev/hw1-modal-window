import './styles/Button.scss';

const Button = ({ type = 'button', classNames, onClick, children }) => (
    <button type={type} className={classNames} onClick={onClick}>
        {children}
    </button>
);

export default Button;
